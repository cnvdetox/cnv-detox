Effective Treatment with Higher Ethical Standards
CNV Detox provides the personal attention to sharpen recovery skills that will be necessary once healing has stabilized and the rest of your life begins. Our dual diagnosis program provides a multi-faceted family approach for effective recovery.

Address: 5919 W 74th St, Los Angeles, CA 90045, USA

Phone: 310-592-0139

Website: https://www.cnvdetox.com
